var ball;

function setup() {
    createCanvas(window.innerWidth - 40, window.innerHeight - 40);
    ball = new Ball(createVector(100, 200));
}
  
function draw() {
    background(255);
    stroke(0);
    strokeWeight(1);    
    ball.move();
    ball.show();
}

function keyReleased() {
    ball.setMoveX(0);
    ball.setMoveY(0);
}

function keyPressed() {
    if (keyCode == LEFT_ARROW) {
        ball.setMoveX(-1);
    } else if (keyCode == RIGHT_ARROW) {
        ball.setMoveX(1);
    } else if (keyCode == UP_ARROW) {
        ball.setMoveY(-1);
    } else if (keyCode == DOWN_ARROW) {
        ball.setMoveY(1);
    }
}
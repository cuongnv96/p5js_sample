function Ball(pos) {
    if (pos) {
        this.pos = pos;
    } else {
        this.pos = createVector();
    }
    this.size = random(5, 15);
    this.dirX = 0;
    this.dirY = 0;

    this.show = function() {
        push();

        stroke(0);
        fill(200);        
        rect(this.pos.x, this.pos.y, this.size, this.size);

        pop();
    }

    this.move = function() {
        this.pos.x += this.dirX; 
        this.pos.y += this.dirY;
    }

    this.setMoveX = function(dirX) {
        this.dirX = dirX;
    }

    this.setMoveY = function(dirY) {
        this.dirY = dirY;
    }
}